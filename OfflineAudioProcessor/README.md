# Offline Audio Processor

## Make out-of-source with cmake

Type the following commands from project's root:
```
mkdir build
cd build
cmake ..
make
```
You can then run the executable with:
```
./OfflineAudioProcessor INPUT OUTPUT
```

## Make out-of-source with gcc

Type the following commands from project's root:
```
mkdir build
cd build
gcc ../main.c ../audio_processor.c -o OfflineAudioProcessor
```
You can then run the executable with:
```
./OfflineAudioProcessor INPUT OUTPUT
```

## Usage

Once built, use the executable with the following command:
```
./OfflineAudioProcessor INPUT OUTPUT
```
`INPUT` must be a raw PCM audio file, mono and float-32. `OUTPUT` will be the same.
