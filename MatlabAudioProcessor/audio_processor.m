%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Offline Audio Processor %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% Parameters

fileName = 'WhatDoes.wav';



%% Read input file

[inputData, sampleRate] = audioread(fileName);

figure(1);
plot(inputData);
grid on;

soundsc(inputData,sampleRate);
pause(length(inputData)/sampleRate);



%% Audio data process

outputData = zeros(size(inputData));

%%%%%%%%%%%%%%%%%%%%%%%%%
% PROCESSING COMES HERE %
%%%%%%%%%%%%%%%%%%%%%%%%%

outputData = inputData;

%%%%%%%%%%%%%%%%%%%%%%%%%



%% Save output

audiowrite('output.wav',outputData, sampleRate);
soundsc(outputData,sampleRate);
figure(2);
plot(outputData);
grid on;