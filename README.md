
# SigMu 2017-2018 - Traitement audio temps-réel sur iOS et Android

---

Thomas Hézard - Lead Audio Scientist

MWM - www.musicworldmedia.com

thomas.hezard@musicworldmedia.com

---


L'objectif de cette séance de travaux pratiques est d'implémenter un module de traitement audio temps-réel sur iOS et sur Android. Nous suivrons pour cela les étapes habituelles d'un tel projet :
* Prototypage de l'algorithme de traitement audio sur Matlab.
* Implémentation "plateform-agnostic" en language C de l'algorithme et test dans un programme simple.
* Intégration du module dans des applications iOS et Android.

Afin de pouvoir explorer les 3 étapes pendant la séance, une grande partie des codes sont fournis. Nous focaliserons ainsi notre travail sur le travail de programmation des traitements audios.


## Fichiers audios d'exemple : `Sounds`

Afin de simplifier l'étape d'implémentation "plateform-agnostic", nous utiliserons des fichiers au format "RAW data". Contrairement à un fichier wav contenant un header décrivant l'encodage des données audios, le RAW n'est qu'une succession de nombre, et il est nécessaire de connaître à l'avance l'encodage des données pour pouvoir les lire. Ici, les fichiers seront tous encodées en PCM, mono et float32 avec une fréquence d'échantillonnage de 44100 Hz.

La conversion des fichiers audios raw vers des fichiers audios standard peut être faite simplement brâce à `ffmpeg`.
Supposons que le fichier `FILE.pcm` est un fichier PCM raw little-endian. Sa conversion en un fichier wav est obtenue avec la commande suivante :
```
ffmpeg -f f32le -ar 44100 -ac 1 -i FILE.pcm FILE.wav
```
Si `FILE.pcm` est un fichiers big-endian, il suffit de remplacer `f32le` par `f32be`.

La conversion inverse (wav vers raw) s'effectue avec la commande suivante :
```
ffmpeg -i FILE.wav -f f32le -acodec pcm_f32le FILE.pcm
```
Pour un fichier big-endian, il suffit de remplacer `f32le` par `f32be`.


## Prototypage Matlab : `MatlabAudioProcessor`

Le but de cette première étape est d'élaborer et tester l'algorithme de traitement audio. Matlab permet d'implémenter et de tester rapidement, et avec un minimum de lignes de code, plusieurs versions de l'algorithme, jusqu'à obtenir un résultat audio satisfaisant.

Un script `audio_processor.m` et quelques fichiers audios d'exemple (dossier `Sounds`, cf commentaires ci-dessus) sont fournis.
Le script ne permet pour l'instant que de lire un fichier audio en entrée, et de recopier les données audios non modifiées dans un fichier audio en sortie.

Il ne vous reste plus qu'à insérer votre algorithme de traitement à l'endroit indiqué et à exécuter le script Matlab pour vérifier le résultat obtenu.

Une fois que votre algorithme est validé, vous pouvez passer à l'étape suivante.


## Implémentation en C : `OfflineAudioProcessor`

Le but de cette seconde étape est de traduire votre algorithme validé dans Matlab en language C compatible avec les contraintes du traitement temps-réel et cross-plateform iOS/Android. Afin de simplifier la tâche, nous réalisons cette étape dans un contexte le plus simple possible. La structure du projet est déjà établie, et le programme permet simplement d'ouvrir un fichier audio au format "RAW data", d'appliquer un traitement sur les données audio dans une boucle de traitement similaire à celle d'un processus temps-réel, et d'écrire le résultat dans un fichier de sortie. Le programme créé n'est donc pas un programme temps-réel, mais nous veillerons à ce que notre code respect les conventions de la programmation audio temps-réel.

Le programme utilise un "objet" `AudioProcessor` (codé dans les fichiers `audio_processor.h` et `audio_processor.c`). Cet objet est utilisé dans le fichier `main.c` de la façon suivante: 
* instantiation et initialisation de l'objet (au démarrage du programme) : ligne 29 dans `main.c`,
* traitement à l'intérieur de la boucle audio : ligne 38 dans `main.c`,
* destruction de l'objet instantié : ligne 47 dans `main.c`.

Les détails pour compiler et utiliser le programme sont donnés dans le README à l'intérieur du dossier.

Pour compléter cette étape, vous devrez modifier les fichiers  `audio_processor.h` et `audio_processor.c` pour que `AudioProcessor` opère le traitement que vous avez validé à l'étape précédente. Pour ceci vous devrez :
* modifier l'implémentation de la fonction `new_audio_processor` (si vous devez modifier son prototype, n'oubliez pas de le faire aussi dans le header et de modifier l'appel de la fonction à la ligne 29 dans `main.c`),
* modifier l'implémentation de la fonction `ap_process`.

Une fois les modifications intégrées et débuggées, vérifier le bon fonctionnement du programme. Vous devez obtenir exactement  le même résutat qu'à l'étape précédente.


## Intégration dans une application modbile : `SampleProject_Android` et `SampleProject_iOS`

Nous allons maintenant intégrer notre algorithme implémenté grâce à l'objet `AudioProcessor` dans une application mobile. L'application que nous nous proposons de développer est très simple : elle capture le son par l'entrée audio par défaut, applique le traitement de `AudioProcessor` sur les données en temps-réel, et envoie les données traitées à la sortie par défaut.

Pour chaque plateforme, un projet d'exemple fourni par le développeur de la plateforme (Google ou Apple) a été modifié pour intégrer les fichiers `audio_processor.h` et `audio_processor.c` développés dans le cadre du projet `OfflineAudioProcessor`.

Pour intégrer votre algorithme, il suffit de remplacer les fichiers `audio_processor.h` et `audio_processor.c` par ceux que vous avez modifiés à l'étape précédente. Ils se trouvent aux endroit suivants :
* Android : `SampleProject_Android/app/src/main/cpp/`
* iOS : `SampleProject_iOS/Classes`

Si besoin, n'oubliez pas de modifier l'appel à la fonction d'initialisation de `AudioProcessor` :
* Android : `SampleProject_Android/app/src/main/cpp/audio_player.cpp` ligne 193
* iOS : `SampleProject_iOS/Classes/AudioController.mm` ligne 301

Pour compiler le projet sur iOS, il suffit d'ouvrir le projet Xcode et de le compiler sur un iPhone/iPod/iPad (cf ci-dessous).

Pour compiler le projet sur Android, il suffit d'ouvrir le projet avec Android Studio et de le compiler sur un téléhpone ou une tablette Android (cf ci-dessous).


## Installation d'Android Studio et compilation

* [Télécharger Android Studio pour votre système](https://developer.android.com/studio/index.html)
* Installer Android Studio.
* Lors du premiet lancement d'Android Studio, demander l'installation de l'API Android 27, de CMake, des Android SDK Tools et du Android NDK.
* Lors de la compilation du projet, en cas d'erreur, il vous sera peut-être demandé l'installation de composants supplémentaires. Installez les composants et redémarrer Android Studio autant de fois que nécessaires.


## Installation d'XCode et compilation

* [Télécharger XCode](https://developer.apple.com/xcode/)
* Installer Xcode.
* Afin de pouvoir compiler sur un appareil physique, un Apple ID est nécessaire. Pour en créer un, suivre [ce lien](https://developer.apple.com/account/).
* Enregistrer le compte Apple dans les préférences d'Xcode.
* Dans les propriétés du projet XCode, créer un Bundle ID unique (n'importe quelle chaîne de caractères).
* La compilation devrait fonctionner. Pour autoriser l'application sur le device, aller dans Réglages -> Général -> Gestion des appareils.

